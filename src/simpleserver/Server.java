// Singleton Method

package simpleserver;

import com.google.gson.Gson;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

class Server {

  //create an object
  private static Server server = new Server();

  //make the constructor private so that this class cannot be
  //instantiated
  private Server() {}

  //Get the only object available

  static Server getInstance() {

    return server;
  }

  static void getTimeResponse(Gson gson, PrintWriter writer) {
    new java.util.Date();
    new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date());

    writer.println("Status: OK");
    writer.println("Timestamp:");
    writer.println(new Date());
  }

  static void showMessage() {
    System.out.println("Singleton pattern created successfully!");
  }
}
