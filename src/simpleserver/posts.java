package simpleserver;

import java.util.HashMap;
import java.util.Map;

public class posts {
  private final static Map<Integer, posts> userid1Dict = new HashMap<>();

  private final int postid;
  private final int userid;
  private final String data;

  public posts(int postid,int userid, String data) {

    this.userid = userid;
    this.postid = postid;
    this.data = data;

    userid1Dict.put(userid, this);
  }

  public static posts getposts(int userid) {
    return userid1Dict.get(userid);
  }

}
