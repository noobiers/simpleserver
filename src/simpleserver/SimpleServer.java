package simpleserver;

import static simpleserver.Server.getTimeResponse;
import static simpleserver.Server.showMessage;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;


class SimpleServer {

  private static int check;
  private static int pmtr;
  private static String[] uriTokens;
  private static String[] uriTokens2;
  private static String[] uriTokens3;


  public static void main(String[] args) throws IOException {
    run();
  }

  private static void run() {
    ServerSocket ding;
    Socket dong = null;
    showMessage();

    try {
      ding = new ServerSocket(1599);
      System.out.println("Opened socket " + 1599);
      while (true) {

        // keeps listening for new clients, one at a time
        try {
          dong = ding.accept(); // waits for client here
        } catch (IOException e) {
          System.out.println("Error opening socket");
          System.exit(1);
        }

        BufferedReader in = getBufferedReader(dong);
        urlParse(in);
        BufferedOutputStream out = new BufferedOutputStream(dong.getOutputStream());
        PrintWriter writer = new PrintWriter(out, true);  // char output to the client
        makeResponse(writer);

        Gson gson = new Gson();
        getTimeResponse(gson,writer);
        getAllPostsOrUsers(gson, writer);
        getPostsResponse(gson, writer);
        getCommentsResponse(gson, writer);

        dong.close();
      }
    } catch (IOException e) {
      System.out.println("Error opening socket");
      System.exit(1);
    }
  }

  private static BufferedReader getBufferedReader(Socket dong) throws IOException {
    InputStream stream = dong.getInputStream();
    return new BufferedReader(new InputStreamReader(stream));
  }

  private static void urlParse(BufferedReader in) {
    uriTokens = null;
    String[] uriTokens1 = null;
    uriTokens2 = null;
    uriTokens3 = null;
    try {

      // read the first line to get the request method, URI and HTTP version
      String line = in.readLine();
      System.out.println("----------REQUEST START---------");
      System.out.println(line);
      String[] tokens = line.split(" ");
      uriTokens = tokens[1].split("\\?");

      // uriTokens1 will have requested parameters
      // check variable checks if there are any parameters specified in the request

      if (uriTokens.length >= 2) {
        uriTokens1 = uriTokens[1].split("&");
        check = 1;
      } else {
        uriTokens1 = uriTokens;
        check = 0;
      }

      //uriTokens2 and uriTokens3 will have parameter names and parameter values

      if (check == 1 && uriTokens1.length == 2) {
        uriTokens2 = uriTokens1[0].split("=");
        uriTokens3 = uriTokens1[1].split("=");
        pmtr = 2;
      } else if (check == 1 && uriTokens1.length == 1) {
        uriTokens2 = uriTokens1[0].split("=");
        pmtr = 1;
      }

      // read only headers
      line = in.readLine();
      while (line != null && line.trim().length() > 0) {
        int index = line.indexOf(": ");
        if (index > 0) {
          System.out.println(line);
        } else {
          break;
        }
        line = in.readLine();
      }
      System.out.println("----------REQUEST END---------\n\n");
    } catch (IOException e) {
      System.out.println("Error reading");
      System.exit(1);
    }
  }

  //----------------------------------------------------------------------------
  //To get all posts or users when no parameters are specified in the request
  //----------------------------------------------------------------------------

  private static void getAllPostsOrUsers(Gson gson, PrintWriter writer) {
    BufferedReader br;// Body of our response
    try {
      br = new BufferedReader(new FileReader("src/data.json"));
      JsonParser jsonParser = new JsonParser();
      JsonObject obj = jsonParser.parse(br).getAsJsonObject();
      System.out.println(uriTokens[0] + "---------");
      if (check == 0) {             // No parameters specified. Enter the if clause
        if (new String("/User").equals(uriTokens[0])) {
          User[] users = gson.fromJson(obj.get("users"), User[].class);
          int count = users.length;
          writer.println("Entries:" + count);
          writer.println("Data:");
          writer.println("<body>" + obj.get("users") + "</body>");

          String jsonString = gson.toJson(users);
          System.out.println(jsonString);
        } else if (!uriTokens[0].equals("") && new String("/posts").equals(uriTokens[0])) {
          posts[] posts = gson.fromJson(obj.get("posts"), posts[].class);
          writer.println("<body>" + obj.get("posts") + "</body>");

          String jsonString = gson.toJson(posts);
          System.out.println(jsonString);
          pmtr = 4;
        }
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  //----------------------------------------------------------------------------
  //To get Posts for requested userid parameters. Parameter can either be 1 or 2.
  //----------------------------------------------------------------------------

  private static void getPostsResponse(Gson gson, PrintWriter writer) {
    if (!uriTokens[0].trim().equals("/Posts") && !uriTokens[0].trim().equals("/posts")) {
      return;
    }
    if (pmtr == 4) {
      return;            // Exit if only posts are requested, not posts of userid
    }
    if (!uriTokens2[0].equals("")) {
      int userId1 = Integer.parseInt(uriTokens2[1]);
      List<Post> postsForUser1 = getAllPostsForUser(gson, writer, userId1);
      System.out.println(userId1);
      for (Post post : postsForUser1) {
        System.out.println(post.getData());
        int count1 = uriTokens.length;
        writer.println("Entries:" + count1);
        writer.println("Data:");
        writer.println("<body>" + gson.toJson(post, Post.class) + "</body>");
      }
    }

    System.out.println("----------------------");
    if (uriTokens3 != null && uriTokens3.length > 0 && !uriTokens3[0].equals("")) {
      int userId2 = Integer.parseInt(uriTokens3[1]);
      List<Post> postsForUser2 = getAllPostsForUser(gson, writer, userId2);
      for (Post post : postsForUser2) {
        System.out.println(post.getData());
        writer.println("<body>" + gson.toJson(post, Post.class) + "</body>");
      }
    }
  }

  private static List<Post> getAllPostsForUser(Gson gson, PrintWriter writer, int userId) {
    BufferedReader br1;// Body of our response
    List<Post> postsForUser = new ArrayList<>();
    try {
      br1 = new BufferedReader(new FileReader("src/data.json"));
      JsonParser jsonParser = new JsonParser();
      JsonObject obj = jsonParser.parse(br1).getAsJsonObject();
      Post[] posts = gson.fromJson(obj.get("posts"), Post[].class);
      for (Post post : posts) {
        if (userId == post.getUserid()) {
          postsForUser.add(post);
        }
      }
    } catch (FileNotFoundException e) {
      // Handle file not found
    }
    return postsForUser;
  }

  //------------------------------------------------------------------------------
  //To get comments for requested postid parameters .Parameter can either be 1 or 2.
  //-------------------------------------------------------------------------------

  private static void getCommentsResponse(Gson gson, PrintWriter writer) {
    if (!uriTokens[0].trim().equals("/comments")) {
      return;
    }
    if (!uriTokens2[0].equals("") && uriTokens2[0]!=null) {
      System.out.println(uriTokens2[0] + "=" + uriTokens2[1]);
      int postId1 = Integer.parseInt(uriTokens2[1]);
      List<comments> commentsForUser1 = getAllCommentsForPost(gson, writer, postId1);
      for (comments comment : commentsForUser1) {
        int count2 = uriTokens2.length;
        writer.println("Entries:" + count2);
        writer.println("Data:");
        writer.println("<body>" + gson.toJson(comment, comments.class) + "</body>");

      }
    }

    System.out.println("----------------------");
    if (uriTokens3 != null && uriTokens3.length > 0 && !uriTokens3[0].equals("")) {
      int postId2 = Integer.parseInt(uriTokens3[1]);
      List<comments> commentsForUser2 = getAllCommentsForPost(gson, writer, postId2);
      for (comments comment : commentsForUser2) {
        writer.println("<body>" + gson.toJson(comment, comments.class) + "</body>");
      }
    }
  }

  private static List<comments> getAllCommentsForPost(Gson gson, PrintWriter writer, int postId) {
    BufferedReader br2;// Body of our response
    List<comments> commentsForUser = new ArrayList<>();
    try {
      br2 = new BufferedReader(new FileReader("src/data.json"));
      JsonParser jsonParser = new JsonParser();
      JsonObject obj = jsonParser.parse(br2).getAsJsonObject();
      comments[] comments = gson.fromJson(obj.get("comments"), comments[].class);
      for (comments commnt : comments) {
        if (postId == commnt.getPostId()) {
          System.out.println("Filtering user id = " + postId);
          commentsForUser.add(commnt);
        }
      }
    } catch (FileNotFoundException e) {
      // Handle file not found
    }
    return commentsForUser;
  }

  //Follows the writer method

  private static void makeResponse(PrintWriter writer) {
    // every response will always have the status-line, date, and server name
    writer.println("HTTP/1.1 200 OK");
    writer.println("Server: TEST");
    writer.println("Connection: close");
    writer.println("Content-type: text/html");
    writer.println("");
  }
}
