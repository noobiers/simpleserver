package simpleserver;

public class comments {
  private Integer postid;
  private Integer userid;
  private String data;

  public comments(int postid, int userid, String data) {
    this.postid = postid;
    this.userid = userid;
    this.data = data;
  }

  Integer getPostId() {
    return postid;
  }

  public Integer getUserId() {
    return userid;
  }

  public String getData() {
    return data;
  }
}

