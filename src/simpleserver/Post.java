package simpleserver;

public class Post {
  private Integer userid;
  private String data;

  public Post(int userid, String data) {
    this.userid = userid;
    this.data = data;
  }

  Integer getUserid() {
    return userid;
  }

  String getData() {
    return data;
  }
}

